/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.interfaceproject;

/**
 *
 * @author MSI
 */
public class testFlyable {
    public static void main(String[] args) {
        
        Bat bat = new Bat("Dum");
        Plane plane = new Plane("Engine Number 1");
        Dog dog = new Dog("Dang");
        Car car = new Car("Engine Number 2");
        Cat cat = new Cat("Maple");
        Human human = new Human("Kantpliw");
        Bird bird = new Bird("Yung");
        
        
        bat.fly();
        plane.fly();
        dog.run();
        car.run();
        cat.run();
        human.run();
        bird.fly();
        
        System.out.println();
        System.out.println();
        
        Flyable[ ] flyable = {bat,plane,bird};
        for (int i = 0; i < flyable.length; i++) {
            if (flyable[i] instanceof Plane) {
                ((Plane) (flyable[i])).startEngine();
                ((Plane) (flyable[i])).raiseSpeed();
                ((Plane) (flyable[i])).run();
                ((Plane) (flyable[i])).fly();
                
            
            } else{
                flyable[i].fly();
            }
            
         
         

        }
        
        System.out.println();
        System.out.println();
        
        Runable[ ] runable = {dog ,plane,human,cat,car};
        for (int i = 0; i < runable.length; i++) {
            if (runable[i] instanceof Plane) {
                ((Plane) (runable[i])).startEngine();
                ((Plane) (runable[i])).raiseSpeed();
                ((Plane) (runable[i])).run();
                ((Plane) (runable[i])).fly();
                
            }
            else if (runable[i] instanceof Car){
                ((Car) (runable[i])).startEngine();
                ((Car) (runable[i])).raiseSpeed();
                ((Car) (runable[i])).run();
                ((Car) (runable[i])).applyBreak();
            }else{
                runable[i].run();
            }
            
            
            
            
            
            
            
        }
}
}